#include <Arduino.h>
#include <Servo.h>

#define RIGHT 1
#define LEFT -1

class NanoMouseMotors{
private:
Servo leftServo;
Servo rightServo;

static const byte power = 250;

public:
void attach(byte leftMotor,byte rightMotor)
{
  leftServo.attach(6);
  rightServo.attach(5);
}
void forward()
{
  leftServo.writeMicroseconds(1500 + power);
  rightServo.writeMicroseconds(1500 - power);
}

void stop(int time = 200)
{
  leftServo.writeMicroseconds(1500);
  rightServo.writeMicroseconds(1500);
  delay(time);
}

void forwardTime(unsigned int time)
{
  forward();
  delay(time);
  stop();
}

void turn(int direction, int degrees)
{
  leftServo.writeMicroseconds(1500 - power * direction);
  rightServo.writeMicroseconds(1500 - power * direction);
  delay(degrees * 4.85 );
  stop();
}

void square ()
{
  for (int x = 0; x < 4; x++)
  {
    turn(LEFT, 90);
    forwardTime(1900);
  }
}

void polygon (int sides)
{
  for (int x = 0; x < 5; x++)
  {
    turn(LEFT,360/sides);
    forwardTime(1900);
  }
}
};